import { IName } from "../Utility/CommonModels";
import { IMove } from "./Move";

export interface IMoveAilment {
  id: number;
  name: string;
  moves: IMove[];
  names: IName[];
}
