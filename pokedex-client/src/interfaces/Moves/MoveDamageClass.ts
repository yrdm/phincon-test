import { IDescription, IName } from "../Utility/CommonModels";
import { IMove } from "./Move";

export interface IMoveDamageClass {
  id: number;
  name: string;
  descriptions: IDescription[];
  moves: IMove[];
  names: IName[];
}
