import { IContestEffect } from "../Contests/ContestEffect";
import { IContestType } from "../Contests/ContestType";
import { ISuperContestEffect } from "../Contests/SuperContestEffect";
import { IGeneration } from "../Games/Generation";
import { IVersionGroup } from "../Games/VersionGroup";
import { IAbilityEffectChange } from "../Pokemon/Ability";
import { IStat } from "../Pokemon/Stat";
import { IType } from "../Pokemon/Type";
import {
  IMachineVersionDetail,
  IName,
  IVerboseEffect,
} from "../Utility/CommonModels";
import { ILanguage } from "../Utility/Language";
import { IMoveAilment } from "./MoveAilment";
import { IMoveCategory } from "./MoveCategory";
import { IMoveDamageClass } from "./MoveDamageClass";
import { IMoveTarget } from "./MoveTarget";

export interface IMove {
  id: number;
  name: string;
  accuracy: number;
  effect_chance: number;
  pp: number;
  priority: number;
  power: number;
  contest_combos: IContestComboSets;
  contest_type: IContestType;
  contest_effect: IContestEffect;
  damage_class: IMoveDamageClass;
  effect_entries: IVerboseEffect[];
  effect_changes: IAbilityEffectChange[];
  flavor_text_entries: IMoveFlavorText[];
  generation: IGeneration;
  machines: IMachineVersionDetail[];
  meta: IMoveMetaData;
  names: IName[];
  past_values: IPastMoveStatValues[];
  stat_changes: IMoveStatChange[];
  super_contest_effect: ISuperContestEffect;
  target: IMoveTarget;
  type: IType;
}

export interface IContestComboSets {
  normal: IContestComboDetail;
  super: IContestComboDetail;
}

export interface IContestComboDetail {
  use_before: IMove[];
  use_after: IMove[];
}

export interface IMoveFlavorText {
  flavor_text: string;
  language: ILanguage;
  version_group: IVersionGroup;
}

export interface IMoveMetaData {
  ailment: IMoveAilment;
  category: IMoveCategory;
  min_hits: number;
  max_hits: number;
  min_turns: number;
  max_turns: number;
  drain: number;
  healing: number;
  crit_rate: number;
  ailment_chance: number;
  flinch_chance: number;
  stat_chance: number;
}

export interface IMoveStatChange {
  change: number;
  stat: IStat;
}

export interface IPastMoveStatValues {
  accuracy: number;
  effect_chance: number;
  power: number;
  pp: number;
  effect_entries: IVerboseEffect[];
  type: IType;
  version_group: IVersionGroup;
}
