import { IVersionGroup } from "../Games/VersionGroup";
import { IDescription, IName } from "../Utility/CommonModels";

export interface IMoveLearnMethod {
  id: number;
  name: string;
  descriptions: IDescription[];
  names: IName[];
  version_groups: IVersionGroup[];
}
