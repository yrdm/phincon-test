import { IDescription } from "../Utility/CommonModels";
import { IMove } from "./Move";

export interface IMoveCategory {
  id: number;
  name: string;
  moves: IMove[];
  descriptions: IDescription[];
}
