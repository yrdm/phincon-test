import { IGeneration } from "../Games/Generation";
import { IPokedex } from "../Games/Pokedex";
import { IVersionGroup } from "../Games/VersionGroup";
import { IName } from "../Utility/CommonModels";
import { ILocation } from "./Location";

export interface IRegion {
  id: number;
  locations: ILocation[];
  name: string;
  names: IName[];
  main_generation: IGeneration;
  pokedexes: IPokedex[];
  version_groups: IVersionGroup[];
}
