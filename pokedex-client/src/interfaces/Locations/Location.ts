import { IGenerationGameIndex, IName } from "../Utility/CommonModels";
import { ILocationArea } from "./LocationArea";
import { IRegion } from "./Region";

export interface ILocation {
  id: number;
  name: string;
  region: IRegion;
  names: IName[];
  game_indices: IGenerationGameIndex[];
  areas: ILocationArea[];
}
