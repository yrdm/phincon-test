import { IItem } from "../Items/Item";
import { ILocation } from "../Locations/Location";
import { IMove } from "../Moves/Move";
import { IPokemonSpecies } from "../Pokemon/PokemonSpecies";
import { IType } from "../Pokemon/Type";
import { IEvolutionTrigger } from "./EvolutionTrigger";

export interface IEvolutionChain {
  id: number;
  baby_trigger_item: IItem;
  chain: IChainLink;
}

export interface IChainLink {
  is_baby: boolean;
  species: IPokemonSpecies;
  evolution_details: IEvolutionDetail[];
  evolves_to: IChainLink[];
}

export interface IEvolutionDetail {
  item: IItem;
  trigger: IEvolutionTrigger;
  gender: number;
  held_item: IItem;
  move: IMove;
  known_move_type: IType;
  location: ILocation;
  min_level: number;
  min_happiness: number;
  min_beauty: number;
  min_affection: number;
  needs_overworld_rain: boolean;
  party_species: IPokemonSpecies;
  party_type: IType;
  relative_physical_stats: number;
  time_of_day: string;
  trade_species: IPokemonSpecies;
  turn_upside_down: boolean;
}
