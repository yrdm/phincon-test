import { IVersionGroup } from "../Games/VersionGroup";
import { IItem } from "../Items/Item";
import { IMove } from "../Moves/Move";

export interface IMachine {
  id: number;
  item: IItem;
  move: IMove;
  version_group: IVersionGroup;
}
