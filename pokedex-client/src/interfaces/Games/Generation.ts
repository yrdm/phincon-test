import { IRegion } from "../Locations/Region";
import { IMove } from "../Moves/Move";
import { IAbility } from "../Pokemon/Ability";
import { IPokemonSpecies } from "../Pokemon/PokemonSpecies";
import { IType } from "../Pokemon/Type";
import { IName } from "../Utility/CommonModels";
import { IVersionGroup } from "./VersionGroup";

export interface IGeneration {
  id: number;
  name: string;
  abilities: IAbility[];
  names: IName[];
  main_region: IRegion;
  moves: IMove[];
  pokemon_species: IPokemonSpecies[];
  types: IType[];
  version_groups: IVersionGroup[];
}
