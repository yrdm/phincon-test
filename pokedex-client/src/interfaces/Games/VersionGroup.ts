import { IRegion } from "../Locations/Region";
import { IMoveLearnMethod } from "../Moves/MoveLearnMethod";
import { IGeneration } from "./Generation";
import { IPokedex } from "./Pokedex";
import { IVersion } from "./Version";

export interface IVersionGroup {
  id: number;
  name: string;
  order: number;
  generation: IGeneration;
  move_learn_methods: IMoveLearnMethod[];
  pokedexes: IPokedex[];
  regions: IRegion[];
  versions: IVersion[];
}
