import { IName } from "../Utility/CommonModels";
import { IBerry } from "./Berry";

export interface IBerryFirmness {
  id: number;
  name: string;
  berries: IBerry[];
  names: IName[];
}
