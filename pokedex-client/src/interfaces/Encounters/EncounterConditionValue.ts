import { IName } from "../Utility/CommonModels";
import { IEncounterCondition } from "./EncounterCondition";

export interface IEncounterConditionValue {
  id: number;
  name: string;
  condition: IEncounterCondition[];
  names: IName[];
}
