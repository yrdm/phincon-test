import { IEncounterConditionValue } from "../Encounters/EncounterConditionValue";
import { IEncounterMethod } from "../Encounters/EncounterMethod";
import { IGeneration } from "../Games/Generation";
import { IVersion } from "../Games/Version";
import { IVersionGroup } from "../Games/VersionGroup";
import { IMachine } from "../Machines/Machine";
import { ILanguage } from "./Language";

export interface ICacheableResource {
  id: number;
  name: string;
}

export interface IDescription {
  description: string;
  language: ILanguage;
}

export interface IEffect {
  effet: string;
  language: ILanguage;
}

export interface IEncounter {
  min_level: number;
  max_level: number;
  condition_values: IEncounterConditionValue;
  chance: number;
  method: IEncounterMethod;
}

export interface IFlavorText {
  flavor_text: string;
  language: ILanguage;
  version: IVersion;
}

export interface IGenerationGameIndex {
  game_index: number;
  generation: IGeneration;
}

export interface IMachineVersionDetail {
  // machine: IApiResource<IMachine>;
  machine: IMachine;
  version_group: IVersionGroup;
}

export interface IName {
  name: string;
  language: ILanguage;
}

export interface IVerboseEffect {
  effect: string;
  short_effect: string;
  language: ILanguage;
}

export interface IVersionEncounterDetail {
  version: IVersion;
  max_chance: number;
  encounter_details: IEncounter[];
}

export interface IVersionGameIndex {
  game_index: number;
  version: IVersion;
}

export interface IVersionGroupFlavorText {
  text: string;
  language: ILanguage;
  version_group: IVersionGroup;
}
