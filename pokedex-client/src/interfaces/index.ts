import { IPokemon } from "./Pokemon/Pokemon";

export * from "./Berries/Berry";
export * from "./Berries/BerryFirmness";
export * from "./Berries/BerryFlavor";
export * from "./Contests/ContestEffect";
export * from "./Contests/ContestType";
export * from "./Contests/SuperContestEffect";
export * from "./Encounters/EncounterCondition";
export * from "./Encounters/EncounterConditionValue";
export * from "./Encounters/EncounterMethod";
export * from "./Evolution/EvolutionChain";
export * from "./Evolution/EvolutionTrigger";
export * from "./Games/Generation";
export * from "./Games/Pokedex";
export * from "./Games/Version";
export * from "./Games/VersionGroup";
export * from "./Items/Item";
export * from "./Items/ItemAttribute";
export * from "./Items/ItemCategory";
export * from "./Items/ItemFlingEffect";
export * from "./Items/ItemPocket";
export * from "./Locations/Location";
export * from "./Locations/LocationArea";
export * from "./Locations/PalParkArea";
export * from "./Locations/Region";
export * from "./Machines/Machine";
export * from "./Moves/Move";
export * from "./Moves/MoveAilment";
export * from "./Moves/MoveBattleStyle";
export * from "./Moves/MoveCategory";
export * from "./Moves/MoveDamageClass";
export * from "./Moves/MoveLearnMethod";
export * from "./Moves/MoveTarget";
export * from "./Pokemon/Ability";
export * from "./Pokemon/Characteristic";
export * from "./Pokemon/EggGroup";
export * from "./Pokemon/Gender";
export * from "./Pokemon/GrowthRate";
export * from "./Pokemon/Nature";
export * from "./Pokemon/PokeathlonStat";
export * from "./Pokemon/Pokemon";
export * from "./Pokemon/PokemonColor";
export * from "./Pokemon/PokemonForm";
export * from "./Pokemon/PokemonHabitat";
export * from "./Pokemon/PokemonShape";
export * from "./Pokemon/PokemonSpecies";
export * from "./Pokemon/Stat";
export * from "./Pokemon/Type";

export interface PaginationParam {
  offset: number;
  limit: number;
}

export interface BuildParam {
  [key: string]: string | number;
}

export interface ApiResultList {
  count: number;
  next: string | null;
  previous: string | null;
  results: ApiResult[];
}

export interface ApiResult {
  name: string;
  url: string;
}

export interface PokemonColorList {
  [key: string]: PokemonColor;
}

export interface PokemonColor {
  text: string;
  bg: string;
}

export interface OwnedPokemon extends IPokemon {
  nickname?: string;
}
