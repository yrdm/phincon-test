import { IBerryFlavor } from "../Berries/BerryFlavor";
import { IMoveBattleStyle } from "../Moves/MoveBattleStyle";
import { IName } from "../Utility/CommonModels";
import { IPokeathlonStat } from "./PokeathlonStat";
import { IStat } from "./Stat";

export interface INature {
  id: number;
  name: string;
  decreased_stat: IStat;
  increased_stat: IStat;
  hates_flavor: IBerryFlavor;
  likes_flavor: IBerryFlavor;
  pokeathlon_stat_changes: INatureStatChange[];
  move_battle_style_preference: IMoveBattleStylePreference[];
  names: IName[];
}

export interface INatureStatChange {
  max_change: number;
  pokeathlon_stat: IPokeathlonStat;
}

export interface IMoveBattleStylePreference {
  low_hp_preference: number;
  high_hp_preference: number;
  move_battle_style: IMoveBattleStyle;
}
