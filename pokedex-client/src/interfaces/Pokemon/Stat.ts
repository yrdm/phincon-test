import { IMove } from "../Moves/Move";
import { IMoveDamageClass } from "../Moves/MoveDamageClass";
import { IName } from "../Utility/CommonModels";
import { ICharacteristic } from "./Characteristic";
import { INature } from "./Nature";

export interface IStat {
  id: number;
  name: string;
  name_index: number;
  is_battle_only: boolean;
  affecting_moves: IMoveStatAffectSets;
  affecting_natures: INatureStatAffectSets;
  characteristics: ICharacteristic;
  move_damage_class: IMoveDamageClass;
  names: IName[];
}

export interface IMoveStatAffectSets {
  increase: IMoveStatAffect[];
  decrease: IMoveStatAffect[];
}

export interface IMoveStatAffect {
  change: number;
  move: IMove;
}

export interface INatureStatAffectSets {
  increase: INature[];
  decrease: INature[];
}
