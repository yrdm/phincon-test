import { IVersion } from "../Games/Version";
import { IVersionGroup } from "../Games/VersionGroup";
import { IItem } from "../Items/Item";
import { ILocation } from "../Locations/Location";
import { IMove } from "../Moves/Move";
import { IMoveLearnMethod } from "../Moves/MoveLearnMethod";
import {
  IVersionEncounterDetail,
  IVersionGameIndex,
} from "../Utility/CommonModels";
import { IAbility } from "./Ability";
import { IPokemonForm } from "./PokemonForm";
import { IPokemonSpecies } from "./PokemonSpecies";
import { IStat } from "./Stat";
import { IType } from "./Type";

export interface IPokemon {
  id: number;
  name: string;
  base_experience: number;
  height: number;
  is_default: boolean;
  order: number;
  weight: number;
  abilities: IPokemonAbility[];
  forms: IPokemonForm[];
  game_indices: IVersionGameIndex[];
  held_items: IPokemonHeldItem[];
  location_area_encounters: string;
  moves: IPokemonMove[];
  sprites: IPokemonSprites;
  species: IPokemonSpecies;
  stats: IPokemonStat[];
  types: IPokemonType[];
}

export interface IPokemonAbility {
  is_hidden: true;
  slot: number;
  ability: IAbility;
}

export interface IPokemonType {
  slot: number;
  type: IType;
}

export interface IPokemonHeldItem {
  item: IItem;
  version_details: IPokemonHeldItemVersion[];
}

export interface IPokemonHeldItemVersion {
  version: IVersion;
  rarity: number;
}

export interface IPokemonMove {
  move: IMove;
  version_group_details: IPokemonMoveVersion[];
}

export interface IPokemonMoveVersion {
  move_learn_method: IMoveLearnMethod;
  version_group: IVersionGroup;
  level_learned_at: number;
}

export interface IPokemonStat {
  stat: IStat;
  effort: number;
  base_stat: number;
}

export interface IPokemonSprites {
  front_default: string;
  front_shiny: string;
  front_female: string;
  front_shiny_female: string;
  back_default: string;
  back_shiny: string;
  back_female: string;
  back_shiny_female: string;
  other: IPokemonSpritesOther;
}

export interface IPokemonSpritesOther {
  dream_world: IPokemonSpritesDreamWorld;
  home: IPokemonSpritesHome;
  "official-artwork": IPokemonSpritesOfficial;
}

export interface IPokemonSpritesDreamWorld {
  front_default: string;
  front_female: string;
}

export interface IPokemonSpritesHome {
  front_default: string;
  front_female: string;
  front_shiny: string;
  front_shiny_female: string;
}

export interface IPokemonSpritesOfficial {
  front_default: string;
}

export interface ILocationAreaEncounter {
  location_area: ILocation;
  version_details: IVersionEncounterDetail[];
}
