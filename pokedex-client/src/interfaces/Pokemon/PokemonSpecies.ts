import { IEvolutionChain } from "../Evolution/EvolutionChain";
import { IGeneration } from "../Games/Generation";
import { IPokedex } from "../Games/Pokedex";
import { IPalParkArea } from "../Locations/PalParkArea";
import { IDescription, IFlavorText, IName } from "../Utility/CommonModels";
import { ILanguage } from "../Utility/Language";
import { IEggGroup } from "./EggGroup";
import { IGrowthRate } from "./GrowthRate";
import { IPokemon } from "./Pokemon";
import { IPokemonColor } from "./PokemonColor";
import { IPokemonHabitat } from "./PokemonHabitat";
import { IPokemonShape } from "./PokemonShape";

export interface IPokemonSpecies {
  id: number;
  name: string;
  order: number;
  gender_rate: number;
  capture_rate: number;
  base_happiness: number;
  is_baby: boolean;
  hatch_counter: number;
  has_gender_differences: boolean;
  forms_switchable: boolean;
  growth_rate: IGrowthRate;
  pokedex_numbers: IPokemonSpeciesDexEntry[];
  egg_groups: IEggGroup;
  color: IPokemonColor;
  shape: IPokemonShape;
  evolves_from_species: IPokemonSpecies;
  evolution_chain: IEvolutionChain;
  habitat: IPokemonHabitat;
  generation: IGeneration;
  names: IName[];
  pal_park_encounters: IPalParkEnounterArea[];
  flavor_text_entries: IFlavorText[];
  form_descriptions: IDescription[];
  genera: IGenus[];
  varieties: IPokemonSpeciesVariety[];
}

export interface IGenus {
  genus: string;
  language: ILanguage;
}

export interface IPokemonSpeciesDexEntry {
  entry_number: number;
  pokedex: IPokedex;
}

export interface IPalParkEnounterArea {
  base_score: number;
  rate: number;
  area: IPalParkArea;
}

export interface IPokemonSpeciesVariety {
  is_default: boolean;
  pokemon: IPokemon;
}
