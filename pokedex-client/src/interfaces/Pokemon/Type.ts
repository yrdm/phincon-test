import { IGeneration } from "../Games/Generation";
import { IMove } from "../Moves/Move";
import { IMoveDamageClass } from "../Moves/MoveDamageClass";
import { IGenerationGameIndex, IName } from "../Utility/CommonModels";
import { IPokemon } from "./Pokemon";

export interface IType {
  id: number;
  name: string;
  damage_relations: ITypeRelations;
  game_indices: IGenerationGameIndex[];
  generation: IGeneration;
  move_damage_class: IMoveDamageClass;
  names: IName[];
  pokemon: ITypePokemon[];
  moves: IMove;
}

export interface ITypePokemon {
  slot: number;
  pokemon: IPokemon;
}

export interface ITypeRelations {
  no_damage_to: Array<IType>;
  half_damage_to: Array<IType>;
  double_damage_to: Array<IType>;
  no_damage_from: Array<IType>;
  half_damage_from: Array<IType>;
  double_damage_from: Array<IType>;
}
