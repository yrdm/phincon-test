import { IName } from "../Utility/CommonModels";
import { IItem } from "./Item";
import { IItemPocket } from "./ItemPocket";

export interface IItemCategory {
  id: number;
  name: string;
  items: IItem[];
  names: IName[];
  pocket: IItemPocket;
}
