import { IItemCategory } from "./ItemCategory";

export interface IItemPocket {
  id: number;
  name: string;
  categories: IItemCategory[];
}
