import { IDescription, IName } from "../Utility/CommonModels";
import { IItem } from "./Item";

export interface IItemAttribute {
  id: number;
  name: string;
  items: IItem[];
  names: IName[];
  descriptions: IDescription[];
}
