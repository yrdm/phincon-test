import { IMove } from "../Moves/Move";
import { IFlavorText } from "../Utility/CommonModels";

export interface ISuperContestEffect {
  id: number;
  name: string;
  flavor_text_entries: IFlavorText[];
  moves: IMove[];
}
