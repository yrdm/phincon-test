import { IBerryFlavor } from "../Berries/BerryFlavor";
import { ILanguage } from "../Utility/Language";

export interface IContestType {
  id: number;
  name: string;
  berry_flavor: IBerryFlavor;
  names: IContestName[];
}

export interface IContestName {
  name: string;
  color: string;
  language: ILanguage;
}
