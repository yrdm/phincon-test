import { atom } from "recoil";
import { ApiResultList, OwnedPokemon } from "../interfaces";
import { recoilPersist } from "recoil-persist";
const { persistAtom } = recoilPersist();

export const apiResultState = atom({
  key: "apiResultState",
  default: [] as ApiResultList[],
});

export const ownedPokemonState = atom({
  key: "ownedPokemonState",
  default: [] as OwnedPokemon[],
  // effects_UNSTABLE: [persistAtom],
});
