import axios from "axios";
import useSWR from "swr";
import { POKEMON_API } from "../constants/api-url";
import { ApiResultList, PaginationParam } from "../interfaces";
import { buildParams } from "../utils/build-params";

export const usePokemons = (params: PaginationParam) => {
  const param = buildParams({
    ...params,
  });

  const { data, error } = useSWR("/api/pokemons", async () => {
    const { data: pokemons } = await axios.get<ApiResultList>(
      `${POKEMON_API}/pokemon?${param}`
    );

    return pokemons;
  });

  return {
    data,
    isLoading: !error && !data,
    isError: error,
  };
};
