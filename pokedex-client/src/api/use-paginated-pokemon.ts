import axios from "axios";
import { POKEMON_API } from "../constants/api-url";
import { ApiResultList } from "../interfaces";
import useSWRInfinite from "swr/infinite";

const fetcher = (url: string) =>
  axios.get<ApiResultList>(url).then((res) => res.data);

export const usePaginatedPokemons = (path: string) => {
  const url = POKEMON_API + path;

  const { data, error, size, setSize } = useSWRInfinite(
    (offset, prevResult: ApiResultList) => {
      if (offset === 0) {
        return `${url}?offset=${offset}&limit=20`;
      }

      if (prevResult && !prevResult.next) return null;

      return prevResult.next;
    },
    fetcher
  );

  const isReachingEnd =
    data?.[0]?.results.length === 0 || data?.[data?.length - 1]?.next === null;
  return {
    data,
    isReachingEnd,
    isLoading: !error && !data,
    isError: error,
    size,
    setSize,
  };
};
