import axios from "axios";
import useSWR from "swr";
import { IPokemon } from "../interfaces";

export const usePokemonDetail = (url: string) => {
  const { data, error } = useSWR(url, async () => {
    const { data: pokemon } = await axios.get<IPokemon>(url);

    return pokemon;
  });

  return {
    data,
    isLoading: !error && !data,
    isError: error,
  };
};
