import { Routes, Route } from "react-router-dom";
import Home from "./pages/Home";
import Inventory from "./pages/Inventory";
import PokemonDetail from "./pages/PokemonDetail";

function App() {
  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/pokemon/:pokemonName" element={<PokemonDetail />} />
      <Route path="/inventory" element={<Inventory />} />
    </Routes>
  );
}

export default App;
