export const nextFibonacci = (n: number) => {
  let a = (n * (1 + Math.sqrt(5))) / 2.0;
  return Math.round(a);
};
