import { BuildParam } from "../interfaces";

export const buildParams = (obj: BuildParam): string => {
  const param = [];
  for (const key in obj) {
    param.push(`${key}=${obj[key]}`);
  }

  return param.join("&");
};
