import { PokemonColorList } from "../interfaces";

export const PokemonColor: PokemonColorList = {
  normal: {
    text: "#A8A77A",
    bg: "#ffffee",
  },
  fire: {
    text: "#EE8130",
    bg: "#ffe6cc",
  },
  water: {
    text: "#6390F0",
    bg: "#daeeff",
  },
  electric: {
    text: "#F7D02C",
    bg: "#fffacb",
  },
  grass: {
    text: "#7AC74C",
    bg: "#e7ffd8",
  },
  ice: {
    text: "#96D9D6",
    bg: "#ebffff",
  },
  fighting: {
    text: "#C22E28",
    bg: "#ffcecc",
  },
  poison: {
    text: "#A33EA1",
    bg: "#f3d7ff",
  },
  ground: {
    text: "#E2BF65",
    bg: "#fffadc",
  },
  flying: {
    text: "#A98FF3",
    bg: "#e9e8ff",
  },
  psychic: {
    text: "#F95587",
    bg: "#ffd5ea",
  },
  bug: {
    text: "#A6B91A",
    bg: "#faffc8",
  },
  rock: {
    text: "#B6A136",
    bg: "#fffbd2",
  },
  ghost: {
    text: "#735797",
    bg: "#ebe5ff",
  },
  dragon: {
    text: "#6F35FC",
    bg: "#d5d3ff",
  },
  dark: {
    text: "#705746",
    bg: "#fff2e7",
  },
  steel: {
    text: "#B7B7CE",
    bg: "#f8faff",
  },
  fairy: {
    text: "#D685AD",
    bg: "#ffe7f9",
  },
};
