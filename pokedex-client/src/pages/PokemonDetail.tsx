import { useState } from "react";
import { useParams } from "react-router-dom";
import { useRecoilState } from "recoil";
import { usePokemonDetail } from "../api/use-pokemon-detail";
import PokemonDetailCard from "../components/PokemonDetailCard";
import { API_URL, POKEMON_API } from "../constants/api-url";
import { ownedPokemonState } from "../stores";
import { toast } from "react-toastify";
import { capitalizeFirstLetter } from "../utils/capitalize-first-letter";
import { randNumber } from "@ngneat/falso";
import CatchButton from "../components/CatchButton";
import axios, { AxiosError } from "axios";
export default function PokemonDetail() {
  let params = useParams();
  const name = params.pokemonName;
  const url = `${POKEMON_API}/pokemon/${name}`;
  const { data, isLoading } = usePokemonDetail(url);

  const [pokemon, setPokemon] = useRecoilState(ownedPokemonState);
  const [isCatching, setIsCatching] = useState(false);
  const [successCatch, setSuccessCatch] = useState(false);

  /**
   * ref: https://stackoverflow.com/questions/44651537/correct-function-using-math-random-to-get-50-50-chance
   */
  const generateRandomProbability = () => (Math.random() < 0.5 ? false : true);

  const handleCatchPokemon = async () => {
    if (!data) return;

    const randTimeout = randNumber({
      min: 500,
      max: 10000,
    });

    if (isCatching) return;
    setIsCatching(true);

    let success: boolean;
    try {
      const {
        data: { catch: res },
      } = await axios.post(`${API_URL}/catch`);
      success = res;
    } catch (err) {
      if (err as AxiosError) {
        /**
         * Fallback to client-side generator.
         */
        success = generateRandomProbability();
      } else {
        setIsCatching(false);
        toast.error("Pokémon escaped!");
        return;
      }
    }

    setTimeout(() => {
      if (success) {
        setSuccessCatch(true);
        setPokemon([
          ...pokemon,
          {
            ...data,
            id: pokemon.length + 1,
            nickname: data.name,
          },
        ]);
        toast.success("Pokemon successfully catched.");
      } else {
        setSuccessCatch(false);
        toast.error(`Uh oh! ${capitalizeFirstLetter(data.name)} has run away.`);
      }
      setIsCatching(false);
    }, randTimeout);
  };

  return (
    <div className="flex h-screen justify-center items-center bg-gray-400">
      {data ? (
        <PokemonDetailCard isLoading={isLoading} {...data}>
          <CatchButton
            onClick={handleCatchPokemon}
            isCatching={isCatching}
            successCatch={successCatch}
          />
        </PokemonDetailCard>
      ) : null}
    </div>
  );
}
