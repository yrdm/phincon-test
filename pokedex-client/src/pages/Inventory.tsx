import { useRecoilState } from "recoil";
import PokemonDetailCard from "../components/PokemonDetailCard";
import { ownedPokemonState } from "../stores";
import { useNavigate } from "react-router-dom";
import ReleaseButton from "../components/ReleaseButton";
import { randNumber } from "@ngneat/falso";
import { useState } from "react";
import { isPrime } from "../utils/is-prime";
import { toast } from "react-toastify";
import axios, { AxiosError } from "axios";
import { API_URL } from "../constants/api-url";

export default function Inventory() {
  const [ownedPokemon] = useRecoilState(ownedPokemonState);
  const [isReleasing, setIsReleasing] = useState(false);

  const navigate = useNavigate();

  const handleRelease = async () => {
    if (isReleasing) return;
    const randTimeout = randNumber({
      min: 500,
      max: 10000,
    });
    setIsReleasing(true);
    let number: number;
    let prime: boolean | undefined;

    try {
      const {
        data: { released },
      } = await axios.post(`${API_URL}/release`);
      prime = released;
    } catch (err) {
      if (err as AxiosError) {
        /**
         * Fallback to client-side generator.
         */
        number = randNumber({ min: 1, max: 1000 });
        prime = isPrime(number);
      } else {
        setIsReleasing(false);
        toast.error("Pokémon refused to be released.");
        return;
      }
    }

    setTimeout(() => {
      if (prime) {
        toast.success("Pokémon is released!");
      } else {
        toast.error("Pokémon refused to be released.");
      }
      setIsReleasing(false);
    }, randTimeout);
  };
  return (
    <>
      {ownedPokemon.length <= 0 ? (
        <div className="flex h-screen justify-center items-center flex-col">
          <span className="font-bold text-gray-400">
            You don't own any Pokémon.
          </span>

          <span
            onClick={() => navigate("/")}
            className="font-semibold text-sm text-blue-400 underline underline-blue-400 cursor-pointer"
          >
            Click here to browse Pokémon list.
          </span>
        </div>
      ) : (
        <div className="grid md:place-items-center grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 gap-2 md:gap-8 lg:gap-15 p-5 ">
          {ownedPokemon.map((pokemon) => {
            return (
              <PokemonDetailCard
                key={pokemon.id}
                isLoading={false}
                useInput={true}
                {...pokemon}
              >
                <ReleaseButton
                  isReleasing={isReleasing}
                  onClick={handleRelease}
                />
              </PokemonDetailCard>
            );
          })}
        </div>
      )}
    </>
  );
}
