import SwrInfiniteSroll from "react-swr-infinite-scroll";
import { usePaginatedPokemons } from "../api/use-paginated-pokemon";
import PokemonCard from "../components/PokemonCard";

function Home() {
  const swr = usePaginatedPokemons("/pokemon");

  const { data, isReachingEnd } = swr;

  return (
    <div className="bg-gray-400 overflow-auto h-screen">
      <SwrInfiniteSroll
        swr={swr}
        isReachingEnd={isReachingEnd}
        loadingIndicator={
          <div className="flex justify-center">
            <div className="u-flash c-loader">
              <img className="h-10 w-10" src="/pokeball.svg" />
            </div>
          </div>
        }
        endingIndicator={
          <div className="flex justify-center items-center p-2">
            <span className="font-bold">
              Congratulations! You reach the end of Pokédex. 🎉
            </span>
          </div>
        }
      >
        <div className="grid md:place-items-center grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 gap-2 md:gap-8 lg:gap-15 p-5 ">
          {data?.map((el) => {
            return el.results.map((pokemon, idx) => {
              return <PokemonCard key={pokemon.url} {...pokemon} />;
            });
          })}
        </div>
      </SwrInfiniteSroll>
    </div>
  );
}

export default Home;
