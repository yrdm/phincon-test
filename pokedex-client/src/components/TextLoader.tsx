interface TextLoaderProps {
  h?: string;
  w?: string;
  p?: string;
}

export default function TextLoader() {
  return (
    <div p="2" h="9" w="20" className="animate-pulse bg-gray-300 rounded"></div>
  );
}
