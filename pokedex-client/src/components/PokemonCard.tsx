import { useNavigate } from "react-router-dom";

import { usePokemonDetail } from "../api/use-pokemon-detail";
import { ApiResult } from "../interfaces";
import { capitalizeFirstLetter } from "../utils/capitalize-first-letter";
import ImageLoader from "./ImageLoader";
import PokemonType from "./PokemonType";
import TextLoader from "./TextLoader";

export default function PokemonCard({ name, url }: ApiResult) {
  const { data, isLoading } = usePokemonDetail(url);
  const navigate = useNavigate();

  const handleCardClick = () => {
    navigate(`/pokemon/${name}`);
  };
  return (
    <div
      onClick={handleCardClick}
      className="bg-white md:w-60 rounded-lg shadow-lg text-center items-center p-5 cursor-pointer"
    >
      <div className="mb-3 ">
        {isLoading ? (
          <ImageLoader />
        ) : (
          <img
            className="mx-auto h-30 w-30 rounded-full "
            src={
              data?.sprites.other["official-artwork"].front_default
                ? data?.sprites.other["official-artwork"].front_default
                : data?.sprites.front_default
            }
            alt={`${name} sprites`}
          />
        )}
      </div>
      <div className="font-semibold text-sm text-gray-400 ">N°{data?.id}</div>

      <div className="font-bold text-lg text-gray-700 mb-3">
        {capitalizeFirstLetter(name)}
      </div>

      <div
        className={`grid gap-2 place-items-center ${
          isLoading
            ? "grid-cols-2"
            : data!.types.length > 1
            ? "grid-cols-2"
            : ""
        }`}
      >
        {isLoading ? (
          <>
            <TextLoader />
            <TextLoader />
          </>
        ) : (
          data?.types.map((el, idx) => {
            return <PokemonType key={idx} type={el.type.name} />;
          })
        )}
      </div>
    </div>
  );
}
