export default function ImageLoader() {
  return (
    <div className="rounded-full animate-pulse delay-500 bg-gray-200 h-30 w-30 mx-auto"></div>
  );
}
