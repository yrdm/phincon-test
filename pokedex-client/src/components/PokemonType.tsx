import { useEffect, useState } from "react";
import { PokemonColor } from "../constants/pokemon-color";

interface PokemonTypeProps {
  type: string;
}

export default function PokemonType({ type }: PokemonTypeProps) {
  const [colors, setColors] = useState({
    text: "",
    bg: "",
  });

  useEffect(() => {
    calculateColor(type);
  }, []);

  const calculateColor = (pokemonType: string) => {
    for (const type in PokemonColor) {
      if (type === pokemonType) {
        setColors(PokemonColor[type]);
      }
    }
  };

  return (
    <span
      className="p-2 h-9 w-20 uppercase items-center justify-center font-bold inline-flex text-md leading-5 font-semibold rounded"
      style={{
        color: colors.text,
        backgroundColor: colors.bg,
      }}
    >
      {type}
    </span>
  );
}
