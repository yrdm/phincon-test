interface CatchButtonProps {
  isCatching: boolean;
  successCatch: boolean;
  onClick: () => void;
}
export default function CatchButton({
  isCatching,
  successCatch,
  onClick,
}: CatchButtonProps) {
  return (
    <div className="flex justify-center items-center pt-5">
      <button
        onClick={onClick}
        className="rounded-lg bg-red-400 p-2 justify-between items-center flex"
      >
        <img
          className={`h-10 w-10 ${isCatching ? "animate-spin" : ""}`}
          src="/pokeball.svg"
        />

        {isCatching ? <span className="font-bold">CATCHING</span> : null}

        {!isCatching && successCatch ? (
          <span className="font-bold">CATCH AGAIN</span>
        ) : null}

        {!successCatch && !isCatching ? (
          <span className="font-bold">CATCH</span>
        ) : null}
      </button>
    </div>
  );
}
