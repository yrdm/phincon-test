interface ReleaseButtonProps {
  onClick: () => void;
  isReleasing: boolean;
}

export default function ReleaseButton({
  isReleasing,
  onClick,
}: ReleaseButtonProps) {
  return (
    <div className="flex justify-center items-center pt-5">
      <button
        onClick={onClick}
        className="rounded-lg bg-red-400 p-2 justify-between items-center flex"
      >
        <img
          className={`h-10 w-10 ${isReleasing ? "animate-bounce" : ""}`}
          src="/pokeball.svg"
        />

        <span className="font-bold">
          {isReleasing ? "RELEASING" : "RELEASE"}
        </span>
      </button>
    </div>
  );
}
