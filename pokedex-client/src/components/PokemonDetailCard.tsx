import { ReactNode } from "react";
import { useRecoilState } from "recoil";
import { IPokemon, OwnedPokemon } from "../interfaces";
import { ownedPokemonState } from "../stores";
import { capitalizeFirstLetter } from "../utils/capitalize-first-letter";
import { useNavigate } from "react-router-dom";
import ImageLoader from "./ImageLoader";
import PokemonType from "./PokemonType";
import TextLoader from "./TextLoader";
import { nextFibonacci } from "../utils/next-fibonacci";
import axios, { Axios, AxiosError } from "axios";
import { API_URL } from "../constants/api-url";

interface PokemonDetailCardProps extends OwnedPokemon {
  isLoading: boolean;
  showInput?: boolean;
  useInput?: boolean;
  children?: ReactNode;
  onShowInput?: () => void;
  onSaveNickname?: () => void;
}

export default function PokemonDetailCard({
  sprites,
  id,
  types,
  name,
  nickname,
  isLoading,
  useInput,
  children,
  ...rest
}: PokemonDetailCardProps) {
  const [ownedPokemon, setOwnedPokemon] = useRecoilState(ownedPokemonState);
  const navigate = useNavigate();

  const handleRename = async () => {
    if (!nickname) return;
    const [currentNickname, seq] = nickname?.split("-");

    let currentSequence = 1;

    if (seq) {
      currentSequence = parseInt(seq);
    }

    let index = 0;

    ownedPokemon.forEach((pokemon, idx) => {
      if (pokemon.id === id) {
        index = idx;
      }
    });

    let nextSequence = currentSequence;

    try {
      const {
        data: { next },
      } = await axios.post(`${API_URL}/rename`, { fibonacci: currentSequence });
      nextSequence = next;
      console.log(next);
    } catch (err) {
      if (err as AxiosError) {
        nextSequence = nextFibonacci(currentSequence);
      }
    }

    setOwnedPokemon([
      ...ownedPokemon.slice(0, index),
      {
        ...rest,
        sprites,
        id,
        types,
        name,
        nickname: `${currentNickname}-${nextSequence}`,
      },
      ...ownedPokemon.slice(index + 1),
    ]);
  };
  return (
    <div className="bg-white md:w-60 rounded-lg shadow-lg text-center items-center p-5">
      <div className="mb-3 ">
        {isLoading ? (
          <ImageLoader />
        ) : (
          <img
            className="mx-auto h-30 w-30 rounded-full "
            src={
              sprites.other["official-artwork"].front_default
                ? sprites.other["official-artwork"].front_default
                : sprites.front_default
            }
            alt={`${name} sprites`}
          />
        )}
      </div>
      <div className="font-semibold text-sm text-gray-400 ">N°{id}</div>

      <div className="flex flex-col items-center justify-center ">
        <div className="space-x-3 items-center justify-center flex">
          <span
            onClick={handleRename}
            className="font-bold text-lg cursor-pointer"
          >
            {nickname
              ? capitalizeFirstLetter(nickname)
              : capitalizeFirstLetter(name)}
          </span>
        </div>

        <span
          onClick={() => navigate("/inventory")}
          className="font-bold text-sm text-gray-500 my-3 cursor-pointer "
        >
          Owned:{" "}
          {ownedPokemon.filter((el: IPokemon) => el.name === name).length}
        </span>
      </div>

      <div
        className={`grid gap-2 place-items-center ${
          isLoading ? "grid-cols-2" : types.length > 1 ? "grid-cols-2" : ""
        }`}
      >
        {isLoading ? (
          <>
            <TextLoader />
            <TextLoader />
          </>
        ) : (
          types.map((el, idx) => {
            return <PokemonType key={idx} type={el.type.name} />;
          })
        )}
      </div>

      {children}
    </div>
  );
}
