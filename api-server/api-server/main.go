package main

import (
	"api-server/controllers"
	"net/http"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)


func main() {
	router := gin.Default()

	router.Use(cors.Default())

	
	router.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"ping": "🏓"})
	})

	router.POST("/catch", controllers.GetCatchProbability)
	router.POST("/release", controllers.GetReleaseProbability)
	router.POST("/rename", controllers.RenamePokemon)

	router.Run()
}