package controllers

import (
	crand "crypto/rand"
	"encoding/binary"
	"log"
	"math"
	rand "math/rand"
	"net/http"

	"github.com/gin-gonic/gin"
)

func GetCatchProbability (c *gin.Context) {
	var src cryptoSource
	rand.New(src)
	
	catch := rand.Float32() < 0.5
	
	c.JSON(http.StatusOK, gin.H{"catch": catch})
}

func GetReleaseProbability (c *gin.Context) {
	var src cryptoSource
	rnd := rand.New(src)
	isPrime := IsPrime(rnd.Intn(1000))

	c.JSON(http.StatusOK, gin.H{"released": isPrime})
}



func RenamePokemon (c *gin.Context) {
	var input NextFibonacciInput


	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	next := NextFibonacci(input.Fibonacci)
	c.JSON(http.StatusOK, gin.H{"next": next})
}

func NextFibonacci(n uint) uint {
	a := float64(n) * (1 + math.Sqrt(5)) / 2.0
	return uint(math.Round(a))
}

func IsPrime(num int) bool {
	if num < 2 {
		 return false
 }
 sq_root := int(math.Sqrt(float64(num)))
 for i:=2; i<=sq_root; i++{
		if num % i == 0 {
			 return false
		}
 }
	return  true
}


type cryptoSource struct{}

type NextFibonacciInput struct {
	Fibonacci uint `json:"fibonacci" binding:"required"`
}

func (s cryptoSource) Seed(seed int64) {}

func (s cryptoSource) Int63() int64 {
    return int64(s.Uint64() & ^uint64(1<<63))
}

func (s cryptoSource) Uint64() (v uint64) {
    err := binary.Read(crand.Reader, binary.BigEndian, &v)
    if err != nil {
        log.Fatal(err)
    }
    return v
}